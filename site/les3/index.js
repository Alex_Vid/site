module.exports = function(page){

  var Menu = require('menu');
  var Material = require('material');
  var templ = require('./templates/default/index');

  var reqlink = '/projects';

  var menu = new Menu();
  var dataMenu = menu.getMenu(reqlink); // данные меню
  var htmlMenu = menu.htmlData(dataMenu);

  var activeItem, activeTitle;

  for(var i=0; i<dataMenu.length; i++){
    if(dataMenu[i].active){
      activeItem = dataMenu[i].name;
      activeTitle = dataMenu[i].title;
      break;
    }
  }

  var mat = new Material();
  var dataMaterial = mat.getMaterial(activeItem); // данные контента или материала


  return templ.replace( new RegExp("#{title}", 'g'), activeTitle)
    .replace( new RegExp("#{menu}", 'g'), htmlMenu)
    .replace( new RegExp("#{material}", 'g'), dataMaterial.htmlData);

};
