const http = require('http');

var templ = require('./../les3/index')();
http.createServer(function(req, res){

	res.writeHead(200, {'Content-Type': 'text/html'});

	res.end(templ);

}).listen(3000);

console.log('Server running at http://127.0.0.1:3000/');
